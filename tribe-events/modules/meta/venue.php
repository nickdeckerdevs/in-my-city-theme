<?php
/**
 * Single Event Meta (Venue) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/venue.php
 *
 * @package TribeEventsCalendar
 * @since 3.6
 */

$postid = get_the_ID();
global $wpdb;
$pre = $wpdb->prefix;
$sql = 'SELECT * FROM ' . $pre . 'imc_venue INNER JOIN ' . $pre . 'imc_events ON ' . $pre . 'imc_venue.id = ' . $pre . 'imc_events.venue_id WHERE event_id = ' . $postid;
$venue_details_imc = $wpdb->get_row( $sql );

if( $venue_details_imc != null ) {
    if( $venue_details_imc->address2 == null || $venue_details_imc->address2 == ' ' || $venue_details_imc->address2 == '' ) {
        $address = $venue_details_imc->address;
    } else {
        $address = $venue_details_imc->address . ' ' . $venue_details_imc->address2;
    }

    $venue_link = '<a href="' . get_site_url() . $venue_details_imc->wp_guid . '">' . addslashes( ucwords( $venue_details_imc->name ) ) . '</a>';
    $venue_address = '<span class="adr"><span class="street-address">' . ucwords( $address ) . '</span>' .
            '<span class="delimiter">,</span>  <span class="locality">' . ucwords( $venue_details_imc->city ) . '</span>' .
            '<span class="delimiter">,</span>  <abbr class="region tribe-events-abbr">' . ucwords( $venue_details_imc->state ) . '</abbr>' .
            '<span class="postal-code">' . $venue_details_imc->zip . '</span>';
    $address_google = str_replace( ' ', '+', trim( $address ) . ' ' . trim( $venue_details_imc->city ) . ' ' . $venue_details_imc->state . ' ' . $venue_details_imc->zip );

    $google_map_link = 'http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . $address_google . '+United+States';
    $venue_phone = $venue_details_imc->phone;
    $venue_website_link = '<a href="' . $venue_details_imc->venue_website . '" target="_blank">' . $venue_details_imc->venue_website . '</a>';
    $venue_map = '<a href="' . $google_map_link . '" target="_blank"> <img class="map-image" src="http://maps.googleapis.com/maps/api/staticmap?center=' . $address_google . '&zoom=12&size=305x305&maptype=roadmap&markers=color:red%7Clabel' . $venue_details_imc->name . '%7C' . $address_google . '" /></a>';

?>

<div class="tribe-events-meta-group tribe-events-meta-group-venue">
	<h3 class="tribe-events-single-section-title"> <?php _e('Venue', 'tribe-events-calendar' ) ?> </h3>
	<dl>
		<?php do_action( 'tribe_events_single_meta_venue_section_start' ) ?>

		<dd class="author fn org"> <?php echo $venue_link ?> </dd>

		<?php
		// Do we have an address?
		echo '<address class="tribe-events-address">' . $venue_address . '</address>';

		// Do we have a Google Map link to display?
		echo '<a href="' . $google_map_link . '">+ Find On Google Maps</a><p>';
		?> <dt> <?php _e( 'Phone:', 'tribe-events-calendar' ) ?> </dt>
			<dd class="tel"> <?php echo $venue_phone; ?> </dd>
			<dt> <?php _e( 'Website:', 'tribe-events-calendar' ) ?> </dt>
			<dd class="url"> <?php echo $venue_website_link; ?> </dd>

		<?php do_action( 'tribe_events_single_meta_venue_section_end' ) ?>
	</dl>
</div>
<script>
    jQuery(document).ready(function($) {
        $('.tribe-events-venue-map').append('<div id="gmap"><?php echo $venue_map; ?></div>'); 
    });
</script>
<?php 
}