<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 * 
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @since  2.1
 * @author Modern Tribe Inc.
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); }

$event_id = get_the_ID();
global $wpdb;
$current = get_the_ID();
$meta = get_post_meta( $event_id );
$sql = 'SELECT approved FROM ' . $wpdb->prefix . 'imc_events WHERE event_id = ' . $current . ' LIMIT 1';
$approved = $wpdb->get_var( $sql );
$approved_class = ($approved == 1) ? '' : ' hidden ';

$content_sql = 'SELECT post_content FROM ' . $wpdb->prefix . 'posts where ID = ' . $current . ' LIMIT 1';
$content = $wpdb->get_var( $content_sql );
if($approved == 1 || current_user_can('manage_options')) { 
    if($approved != 1) {
?> <h2>This event is not yet approved</h2> 
    <?php } ?>

<div id="tribe-events-content" class="tribe-events-single">
    <p class="tribe-events-back"><a href="<?php echo tribe_get_events_link() ?>"> <?php _e( '&laquo; All Events', 'tribe-events-calendar' ) ?></a></p>
        
	<!-- Notices -->
	<?php tribe_events_the_notices() ?>
        <?php the_title( '<h2 class="tribe-events-single-event-title summary">', '</h2>' ); ?>
        <?php 
        
        $band_id = imc_get_band_id( 'event_id', $event_id );
        $query_var_name = get_query_var( 'name' );
        $venue_id_return = imc_get_event_venue_id( $query_var_name );
        if( $venue_id_return != null ) {
            $venue = imc_get_venue_details_event( $venue_id_return );
            $venue_link = get_permalink( $venue->wp_post_id );
        } else {
            $venue = imc_get_venue_event_details_event( $event_id );
            $venue_link = '';
        }
        
        $street = trim( $venue->address . ' ' . $venue->address2 ); 
        $address = $street . ', ' . $venue->city; 
        $map_url = 'http://maps.google.com/?q=' . $address . ' ' . $venue->state . ' ' . $venue->zip; ?>
        <h3>Address: <a href="<?php echo $venue_link;  ?>"><?php echo $venue->name; ?></a> | <a href="<?php echo $map_url ?>" target="_blank"><?php echo $address; ?></a></h3>
        
        <div class="tribe-events-schedule updated published tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h3>', '</h3>'); ?>
		
			<span class="tribe-events-divider">|</span>
			<span class="tribe-events-cost"><?php echo get_post_meta( $current, '_EventCost', true ); ?></span>
		
	</div>

	<!-- Event header -->
	<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
		<ul class="tribe-events-sub-nav">
			<!--<li class="tribe-events-nav-previous"><?php /* tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) */ ?></li>
			<li class="tribe-events-nav-next"><?php /* tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) */ ?></li>-->
		</ul><!-- .tribe-events-sub-nav -->
	</div><!-- #tribe-events-header -->
        
	<?php while ( have_posts() ) :  the_post(); ?>
        <?php
                    global $wpdb;
                    $current = get_the_ID();
                    $sql = 'SELECT approved FROM ' . $wpdb->prefix . 'imc_events WHERE event_id = ' . $current . ' LIMIT 1';
                    $approved = $wpdb->get_var( $sql );
                    $approved_class = ($approved == 1) ? '' : ' hidden ';
                    
                if($approved == 1 ) {
                ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('vevent'); ?>>
			<!-- Event featured image -->
			<?php 
                        
                        if( tribe_event_featured_image() == '' ) {
                            global $wpdb;
                            $sql = 'SELECT image FROM ' . $wpdb->prefix . 'imc_events WHERE event_id = ' . $event_id;
                            $image_url = $wpdb->get_var($sql);
                            $image = '<div class="tribe-events-event-image" style="text-align: center;"><img src="' . $image_url . '" /></div>'; 
                        } else {
                            $image = tribe_event_featured_image();
                        }
                        echo $image; ?>

			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content entry-content description">
				<?php the_content(); ?>
			</div><!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
                        
			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
                        
				<?php  echo tribe_get_template_part( 'modules/meta' ) ?>
                        <div id="map-that-moves">
                            <a href="<?php echo $map_url; ?>" target="_blank">
                                <img class="map-image" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $address; ?>&amp;zoom=15&amp;size=450x250&amp;maptype=roadmap&amp;markers=color:red%7Clabel<?php echo $venue->name; ?>%7C<?php echo $address; ?>">
                            </a>
                        </div>
                        
			<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
		
                </div><!-- .hentry .vevent -->
                <?php
                } ?>
		<?php if( get_post_type() == TribeEvents::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>
                        
	<!-- Event footer -->
    <div id="tribe-events-footer">
		<!-- Navigation -->
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
		<ul class="tribe-events-sub-nav">
			<!--<li class="tribe-events-nav-previous"><?php /* tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) */ ?></li>
			<li class="tribe-events-nav-next"><?php /* tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) */ ?></li>-->
		</ul><!-- .tribe-events-sub-nav -->
	</div><!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->
<?php } else {
    ?> <h2>This event is not yet approved</h2> <?php
}
?>

<script>
jQuery(document).ready(function($) {
    function formatPhoneNumber(number) {
        var phone = number.replace(/[^\d]/g, "");
        return "+" + phone;
    }
    var phone = '<?php echo $meta['_EventPhone'][0]; ?>';
    $('ul.uk-navbar-nav.uk-hidden-small>li:eq(1)').addClass('uk-active');     
    var map = $('#map-that-moves').html();
    $('.tribe-events-meta-group.tribe-events-meta-group-details').append('<div id="event-map">' + map + '</div>');
    $('#map-that-moves').remove();
    if(phone != 'null') {
        $('.tribe-events-meta-group.tribe-events-meta-group-details dl').append('<dt>Phone:</dt><dd><a href="tel:'+formatPhoneNumber(phone)+'">'+(phone)+'</a></dd>')
    }
    if($('.tribe-events-cost').html() === 'Paid') {
        $('dd.tribe-events-event-cost').html('Paid');
    }
   
   
});
</script>
<a class="create-event-button" href="<?php echo get_site_url(); ?>/events/create-event">Add An Event</a>