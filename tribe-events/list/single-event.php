<?php 
/**
 * This is the page that displays like a search like this page: http://napleslivenow.com/events/?action=tribe_list&tribe_paged=1&tribe_event_display=list&tribe-bar-search=fine+arts
 * 
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 * @since  3.0
 * @author Modern Tribe Inc.
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); } ?>

<?php 

$current = get_the_ID();
$postid = get_the_ID();
global $wpdb;
$imc_event_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_events WHERE event_id = ' . $postid;
$imc_event_details = $wpdb->get_row( $imc_event_sql );
$band_id = imc_get_band_id( 'event_id', $postid );
$meta = get_post_meta( $postid );
if( $imc_event_details->venue_id != 0 ) {
    $venue_details = imc_get_venue_details_event( $imc_event_details->venue_id );
} else {
    $venue_details = imc_get_venue_event_details_event( $postid );
}

?>
<!-- Event Cost -->
<?php if ( tribe_get_cost() ) : ?> 
	<div class="tribe-events-event-cost">
            <span><?php echo get_post_meta( $current, '_EventCost', true ); ?></span>
	</div>
<?php endif; ?>

<!-- Event Title -->
<?php do_action( 'tribe_events_before_the_event_title' ) ?>
<h2 class="tribe-events-list-event-title entry-title summary">
	<a class="url" href="<?php echo tribe_get_event_link() ?>" title="<?php the_title() ?>" rel="bookmark">
            <?php 
            $title = the_title( '', '', false ); 
            echo ucwords( $title ); ?>
        </a>
</h2>
<?php do_action( 'tribe_events_after_the_event_title' ) ?>

<!-- Event Meta -->
<?php do_action( 'tribe_events_before_the_meta' ) ?>
<div class="tribe-events-event-meta vcard"> <div class="author">

	<!-- Schedule & Recurrence Details -->
	<div class="updated published time-details">
		<?php echo tribe_events_event_schedule_details() ?>
	</div>

	
		

</div> </div><!-- .tribe-events-event-meta -->
<?php do_action( 'tribe_events_after_the_meta' ) ?>
<!-- Event Image -->


<!-- Event Content -->
<?php do_action( 'tribe_events_before_the_content' ) ?>
<div class="tribe-events-list-event-description tribe-events-content description entry-summary">
	<?php the_excerpt() ?>
        
	<a href="<?php echo tribe_get_event_link() ?>" class="tribe-events-read-more" rel="bookmark"><?php _e( 'Find out more', 'tribe-events-calendar' ) ?> &raquo;</a>
        
       
</div><!-- .tribe-events-list-event-description -->

<script>
jQuery(document).ready(function($) {
   $('ul.uk-navbar-nav.uk-hidden-small>li:eq(1)').addClass('uk-active');     
});
</script>
    <?php do_action( 'tribe_events_after_the_content' );
