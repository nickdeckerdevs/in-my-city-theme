/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function get_url_parameter( param ) {
    var url = window.location.search.substring(1);
    var url_vars = url.split('&');
    for(var i=0; i < url_vars.length; i++) {
        var param_name = url_vars[i].split('=');
        if(param == param_name[0]) {
            return param_name;
        }
    }
}

function display_modal_alert( $message ) {
    var $height = jQuery('body').height();
    var $width = jQuery('body').width();
    jQuery('body').append('<div id="modal-background" style="position: fixed; top: 0; left: 0; width: ' + $width + 'px; height: ' + $height + 'px; background: rgba(0,0,0,0.8);"><div id="temp-alert" style="position: fixed; top: 50%; left: 50%; width: 300px; margin-left: -150px;padding: 20px; border-radius: 10px; background: #fff; color: #6fa032;"><p style="text-align: center;margin-bottom: 0;">' + $message + '<br><br>Click Anywhere to return to results</div></div>');
    setTimeout(function() {
        remove_filter_alert();
    }, 4000);
}
function display_modal_background() {
    var $height = jQuery('body').height();
    var $width = jQuery('body').width();
    jQuery('body').append('<div id="modal-background" style="position: fixed; top: 0; left: 0; width: ' + $width + 'px; height: ' + $height + 'px; background: rgba(0,0,0,0.8);"></div>');
}

function remove_filter_alert() {
    jQuery('#modal-background').remove();
}
                
function display_loading() {
    var $height = jQuery('body').height();
    var $width = jQuery('body').width();
    jQuery('body').append('<div id="modal-loading" style="position: fixed; top: 0; left: 0; width: ' + $width + 'px; height: ' + $height + 'px; background: rgba(0,0,0,0.8);"><div id="temp-alert" style="position: fixed; top: 50%; left: 50%; width: 300px; margin-left: -150px;padding: 20px; border-radius: 10px; background: #fff; color: #6fa032;"><p style="text-align: center;margin-bottom: 0;">Your results are loading...<br><br>Not finding what you\'re looking for?<br>Try clearing your search.</div></div>');
}

function remove_loading() {
    jQuery('#modal-loading').remove();
}
var bun = '<span class="icon-bar"></span>';
var patty = '<span class="icon-bar"></span>';

function mobile_menu(e) {
    var element = $(e).find('span');
    if(element.text() == 'Naples Live Now - Open Menu') {
//        console.log('true');
    }
}

function search_for_free_events() {
    jQuery('#tribe-bar-search').val('free');
    jQuery('.tribe-events-button').trigger('click');
//    console.log('here');
}

jQuery(document).ready(function($) {
//    $('.uk-hidden-small').hide();
    $('.uk-navbar-nav').before('<div id="mobile-container"><span>Naples Live Now - Open Menu</span><div id="mobile-menu">' + bun + patty + bun + '</div></div>');
    $('body').on('click', '#mobile-container', function() {
        element = $(this).find('span').eq(0);
        if(element.text() == 'Naples Live Now - Open Menu') {
            element.text('Naples Live Now - Close Menu');
            display_modal_background();
            $('.uk-navbar-nav').animate({ right: 0 }, 1000, function() {
                $('.uk-hidden-small').show();
                $('body').on('click', '#modal-background', function() {
                    remove_filter_alert();
                    element.text('Naples Live Now - Open Menu');
                    $('.uk-navbar-nav').css('right', '-300px');
                    $('.uk-hidden-small').hide();
                });
            });
            
        } else {
            element.text('Naples Live Now - Open Menu');
            $('.uk-navbar-nav').animate({ right: '-300px' }, 300, function() {
                remove_filter_alert();
                $('.uk-hidden-small').hide();
            });
        }
    });
    if($('.imc-restaurant').length >= 1) {
        $('.tm-content').before('<div id="goto-filters">Filter Results &nbsp;</div>');
        $('body').on('click', '#goto-filters', function() {
            $('html, body').animate({
                scrollTop: $('#imc-filter-venues').offset().top
            });
        });

    }
    $('.tribe-events-page-template').on('click', '.get-free-button', function() {
        jQuery('#tribe-bar-search').val('free');
        jQuery('.tribe-events-button').trigger('click');
    });
    $('body').on('click', '#modal-background', function() {
       remove_filter_alert();
    });
    if($('body').hasClass('single')) {
        var directionsLink = $('#directions-container').html();
        $('.uk-article-title').after(directionsLink);
    
    }
    
    $('body').on('click', '#temp-alert', function() {
       remove_filter_alert();
    });
    $('#imc-filter-venues').on('click', 'input[type="checkbox"]', function(e) {
        get_new_results(e);
    });
    
    $('#imc-filter-venues').on('change', 'select', function(e) {
        get_new_results(e);
    });
    
    $('#imc-filter-venues').on('click', '#namesearch', function(e) {
         get_new_results(e);
    }); 
    $('body').on('click', '#name-search', function(e) {
//        console.log($('#venue-search-name').val());
        $('#venue-name-search').val($('#venue-search-name').val());
        get_new_results(e);
    });
    $('body').on('click', '.pagination-button', function(e) {
//        console.log($(this).data('pagination'));
        $('#pagination-record').val($(this).data('pagination'));
        get_new_results(e);
    });
    $('body').on('click', '.pagination-letter', function(e) {
//        console.log($(this).data('pagination'));
        $('#pagination-letter').val($(this).data('pagination'));
        get_new_results(e);
    });
    $('body').on('click', '.clear-all', function(e) {
        $('#pagination-letter').val($(this).data('pagination'));
        $('#imc-filter-venues').trigger('reset');
        get_new_results(e);
    });
    $('body').on('keyup', '#venue-search-name', function(e) {
        if(e.keyCode == 13){
//            console.log('hit enter');
            $("#name-search").click();
        }
    });
    

    function get_new_results(e) {
        e.stopPropagation();
        data = $('#imc-filter-venues').serialize();
        display_loading();
//        console.log(data);
        $.post(ajaxurl, data, function (response) {
            var $previous_total = $('#result-total').text();
            $('.tm-content').html(response);
            if($('#venue-search-hidden').val() == 'band') {
                var total_results = $('.band').length;
            } else {
                var total_results = $('.venue').length;
            }
            
            if(total_results > 1 || total_results == 0) {
                var plural = 's';
            } else {
                var plural = '';
            }
            $('.die').html('<div class="center">Showing <span id="result-total">' + total_results + '</span> result' + plural + '.</div>');
            remove_loading();
            window.scrollTo(0,0);
            if($('#pagination').hasClass('hidden')) {
                $('.die').before('<div id="pagination">' + $('#pagination').html() + '</div>');
            }
            $('#venue-search-name').val($('#venue-name-search').val());
        });
        
    }
    if(window.location.search.length > 1 && window.location.search.indexOf('type') > 0) {
        id = get_url_parameter( 'id' );
        type = get_url_parameter( 'type' );
        $('#' + type[1]).trigger('click');
    }
    
    
    
    if(window.location.search.indexOf('claim') > 0) {
        var str = window.location.search.split('=');
        jQuery(document).ready(function() {
            jQuery('#ninja_forms_field_7').val(str[1]);
        });
        
    }    
});